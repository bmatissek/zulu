package zulu.operacoes.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import zulu.auth.domain.UsuarioRepository
import zulu.operacoes.domain.Operacao
import zulu.operacoes.domain.OperacaoRepository
import kotlin.Exception

@RestController
@RequestMapping("/api/v2/")
class OperacaoRestController @Autowired constructor(
        private val operacaoRepository: OperacaoRepository,
        private val usuarioRepository: UsuarioRepository
) {

    @GetMapping("/operacoes")
    suspend fun getAll(@RequestParam("usuarioCriador") idUsuario: String?): List<Operacao> {

        val operacoes = operacaoRepository.getAll().toMutableList()

        idUsuario?.let {
            operacoes.retainAll { op -> op.usuarioCriador == idUsuario }
        }

        return operacoes

    }

    @GetMapping("/operacoes/{id}")
    suspend fun getById(@PathVariable("id") id: String): Operacao {
        return operacaoRepository.getById(id) ?: throw OperacaoNaoEncontradaException()
    }


    @PutMapping("/operacoes/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    suspend fun upsert(@PathVariable("id") id: String, @RequestBody operacao: Operacao) {

        usuarioRepository.getById(operacao.usuarioCriador) ?: throw UsuarioNaoEncontradoException()

        operacaoRepository.upsert(operacao)
    }

}

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Operação não encontrada.")
class OperacaoNaoEncontradaException : Exception()

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O Usuário referenciado no corpo da operação não foi encontrado.")
class UsuarioNaoEncontradoException : Exception()