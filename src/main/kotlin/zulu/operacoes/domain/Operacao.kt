package zulu.operacoes.domain

data class Operacao(
        val id: String,
        val nome: String,
        val descricao: String,
        val usuarioCriador: String
)