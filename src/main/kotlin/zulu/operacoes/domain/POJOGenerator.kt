package zulu.operacoes.domain

import zulu.auth.domain.usuario1
import zulu.auth.domain.usuario2

/**
 * Gerador de POJOS de Operações Tabajara.
 * Use para seus testes (por sua conta e risco).
 **/

fun operacao1(): Operacao = Operacao(
        "OP1",
        "Nome da Operação 1",
        "Descrição da Operação 1",
        usuarioCriador = usuario1().id
)

fun operacao2(): Operacao = Operacao(
        "OP2",
        "Nome da Operação 2",
        "Descrição da Operação 2",
        usuarioCriador = usuario2().id

)

fun operacao3(): Operacao = Operacao(
        "OP3",
        "Nome da Operação 3",
        "Descrição da Operação 3",
        usuarioCriador = usuario2().id

)