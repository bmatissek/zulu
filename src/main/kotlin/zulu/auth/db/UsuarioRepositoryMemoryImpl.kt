package zulu.auth.db

import org.springframework.stereotype.Repository
import zulu.auth.domain.UsuarioRepository
import zulu.auth.domain.Usuario
import zulu.auth.domain.usuario1
import zulu.auth.domain.usuario2


@Repository
class UsuarioRepositoryMemoryImpl : UsuarioRepository {
    private object UsuarioSingleton {
        val usuarioMap : MutableMap<String, Usuario> = mutableMapOf(
                usuario1().id to usuario1(),
                usuario2().id to usuario2()
        )
    }

    override fun getAll(): List<Usuario> = UsuarioSingleton.usuarioMap.values.toList()

    override fun getById(id : String): Usuario? = UsuarioSingleton.usuarioMap[id]

    override fun upsert(usuario: Usuario) {
        UsuarioSingleton.usuarioMap[usuario.id] = usuario
    }
}




